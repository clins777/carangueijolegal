package br.ufpe.voxar.webcrabs;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.GeolocationPermissions.Callback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends Activity
{
	private WebView mWebView;

	public class GeoWebViewClient extends WebViewClient
	{
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url)
		{
			Log.i("DEBUG", "view " + view.toString());
			Log.i("DEBUG", "url: " + url);
			view.loadUrl(url);
			return true;
		}
	}

	public class GeoWebChromeClient extends WebChromeClient
	{
		@Override
		public void onGeolocationPermissionsShowPrompt(String origin,
				Callback callback)
		{
			Log.i("DEBUG", "ORIGIN: " + origin);
			Log.i("DEBUG", "CALLBACK: " + callback.toString());
			callback.invoke(origin, true, false);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mWebView = (WebView) findViewById(R.id.webview);
		mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		//mWebView.getSettings().setBuiltInZoomControls(true);
		mWebView.setWebViewClient(new GeoWebViewClient());
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		mWebView.getSettings().setGeolocationEnabled(true);
		mWebView.setWebChromeClient(new GeoWebChromeClient());
		mWebView.loadUrl("file:///android_asset/index.html");

	}

	@Override
	public void onBackPressed()
	{
		if(mWebView.canGoBack())
			mWebView.goBack();
		else
			super.onBackPressed();
	}
	//	WebView myBrowser;
	//
	//	/** Called when the activity is first created. */
	//	@Override
	//	public void onCreate(Bundle savedInstanceState) {
	//		super.onCreate(savedInstanceState);
	//		setContentView(R.layout.main);
	//		myBrowser = (WebView) findViewById(R.id.mybrowser);
	//		myBrowser.addJavascriptInterface(new MyJavaScriptInterface(this), "AndroidFunction");
	//		myBrowser.getSettings().setJavaScriptEnabled(true);  
	//		myBrowser.loadUrl("file:///android_asset/mypage.html");
	//
	//	}
	//
	//	public class MyJavaScriptInterface {
	//		Context mContext;
	//
	//		MyJavaScriptInterface(Context c) {
	//			mContext = c;
	//		}
	//
	//		public void showToast(String toast){
	//			Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
	//		}
	//
	//		public void openAndroidDialog(){
	//			AlertDialog.Builder myDialog 
	//			= new AlertDialog.Builder(MainActivity.this);
	//			myDialog.setTitle("DANGER!");
	//			myDialog.setMessage("You can do what you want!");
	//			myDialog.setPositiveButton("ON", null);
	//			myDialog.show();
	//		}
	//
	//	}

}
